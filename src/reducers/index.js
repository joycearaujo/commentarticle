//const INITIAL =  {person:{name: 'John'}, count: 0};

const INITIAL = {article:{title: 'Redux-Saga tutorial', descript: 'Build a simple app that fetches dog images using React, Redux & Redux-Saga'},comments: [], count: 0};

const reducer = (state = INITIAL, action) =>{
    switch(action.type){
        case 'ADD_COMMENT':
            console.log(action.payload);
            return{
                ...state, count: state.count += 1, comments: state.comments.concat(action.payload)
            }
        default:
            return state;
    }
}

export default reducer