import React, {Component} from 'react';
import {SafeAreaView, ScrollView} from 'react-native';

import Comments from './Comments';
import Article from './Article';
import Footer from './Footer';

function Home(){
    return(
        <SafeAreaView style={{flex: 1}}>
            <ScrollView>
                <Article/>
                <Comments/>
                <Footer/>
            </ScrollView>
        </SafeAreaView>
    );
}

export default Home;
