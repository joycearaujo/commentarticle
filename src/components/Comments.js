import React from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';

import {connect} from 'react-redux';

function Comments ({comments}){
    return(
        <View style={styles.container}>
            <Text style={styles.title}>Comments</Text>
            <View style={styles.viewComments}>
                {comments.map((item, i) => {
                    return(
                        <View style={styles.viewComment} key={i}>
                            <Image source={require('../assets/default.jpg')} style={styles.image}/>
                            <Text style={styles.comment} >{item.descript}</Text>
                        </View>
                    )
                })}
            </View>
            
        </View>
    );

}

const mapStateToProps = state => {
    return{
        comments: state.comments,
    }
}

export default connect(mapStateToProps)(Comments);

const styles = StyleSheet.create({
    container:{
        flex: 1,
    },
    title:{
        fontSize: 18,
        fontWeight: '200',
        alignSelf: 'center',
        marginTop: 20,
        marginBottom: 10
    },
    viewComments:{
        borderTopWidth: 1,
        borderTopColor: '#000'
    },
    viewComment:{
        flex:1,
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10,
        padding: 15,
    },
    comment:{
        fontSize: 15,
        marginLeft: 15
    }, 
    image:{
        width: 80, 
        height: 80,
        borderRadius: 50
    }
})