import React, {Component} from 'react';
import {View, Text, StyleSheet, Image, TextInput, TouchableHighlight} from 'react-native';

import {connect} from 'react-redux'

class Article extends Component{

    constructor(props){
        super(props);
        this.state ={
            input :'',
            vazio: false
        }

        this.handleSend = this.handleSend.bind(this);
        this.handleBlur = this.handleBlur.bind(this);

    }

    handleSend(){
        if(this.state.input == ''){
            this.setState({vazio: true});
            return;
        }
        this.setState({vazio: false})
        this.props.ADD({id: Math.random(), descript: this.state.input});
    };

    handleBlur(){
        if(this.state.input == ''){
            this.setState({vazio: true});
        }else{
            this.setState({vazio: false})
        }   
    }

    render(){
        return(
            <View style={styles.container}>
                <View style={styles.viewImage}>
                    <Image source={require('../assets/redux-saga.png')} style={{width: 300}}/>
                </View>
                <Text style={styles.title}>{this.props.article}</Text>
                <Text style={styles.text}>{this.props.descript}</Text>
                
                <TextInput onBlur={this.handleBlur}  style={[styles.input, {borderColor: this.state.vazio ? 'red' : '#5bb850'}]} underlineColorAndroid = "transparent" placeholder='Comment' value={this.state.input} onChangeText={(text) => this.setState({input: text})}/>
                {this.state.vazio && <Text style={styles.small}>Preencha este campo</Text>}
    
                <TouchableHighlight style={styles.button} onPress={this.handleSend}>
                    <View style={styles.areaButton}>
                        <Text style={styles.textButton}>Send Comment</Text>
                    </View>
                </TouchableHighlight>
    
            </View>
        );
    }
}

const mapStateToProps = state =>{
    return{
        article : state.article.title,
        descript : state.article.descript
    }
}

const mapDispatchToProps = dispatch =>{
    return{
        ADD: (val) => dispatch({type: 'ADD_COMMENT', payload: val})
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Article);

const styles = StyleSheet.create({
    container:{

    },
    title:{
        fontSize: 20,
        fontWeight: 'bold',
        color: '#999',
        alignSelf: 'center',
        padding: 10
    },
    text:{
        fontSize: 16,
        color: '#000',
        marginLeft: 10, 
        marginRight: 10,
        marginBottom: 20
    }, 
    viewImage:{
        alignSelf: 'center', 
        paddingTop: 30
    },
    input:{
        marginHorizontal: 15,
        marginBottom: 15,
        height: 40,
        borderWidth: 1,
        borderRadius: 15,
        padding: 10
    },
    button:{
        marginHorizontal: 15,
        height: 40,
        backgroundColor: '#5bb850',
        borderRadius: 15,
    },
    areaButton:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textButton:{
        fontSize: 15,
        fontWeight: 'bold',
        color: '#FFF',
        justifyContent: 'center',
        alignItems: 'center'

    },
    small:{
        fontSize: 13,
        color: 'red',
        marginHorizontal: 15,
        marginTop: -17,
        marginBottom: 10

    }
})