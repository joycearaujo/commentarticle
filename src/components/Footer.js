import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

import {connect} from 'react-redux';

function Footer({count}){
    return(
        <View>
            <Text style={styles.text}>{count} {count > 1 ? 'Comentários' : 'Comentário' }</Text>
        </View>
    );
}

const mapStateToProps = state =>{
    return{
        count: state.count
    }
    
}

export default connect(mapStateToProps)(Footer);

const styles = StyleSheet.create({
    text:{
        fontSize: 30,
        alignSelf: 'center'
    }
})
