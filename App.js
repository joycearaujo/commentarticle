/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';

import {Provider} from 'react-redux';

import store from './src/store/index';
import Home from './src/components/Home';

const App = () =>{
    return(
      <Provider store={store}>
        <Home/>
      </Provider>
    );
};

export default App;
